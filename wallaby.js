module.exports = function (wallaby) {
    return {
      files: [
        'src/logic/**/*.ts',
      ],
  
      tests: [
        'test/logic/**/*.spec.ts'
      ],

      "env": {
        "type": "node",
        'runner': 'node'
      },
      compilers: {
        'src/**/*.ts': wallaby.compilers.typeScript({ module: 'commonjs' }),
      },

      setup: function(){
        global.logicToSrcDir = './../../../../src'
      },
      testFramework: 'jest',
  
      // debug: true
    };
  };