import './../../src/logic/Commands/columnCmd';
import '../../src/logic/logic'
import { getCommands } from '../../src/logic/logic';
import TableColumnPair, { Schema, Table, Column } from '../../src/logic/Classes';
import { columnCmdHandlerFunc } from './../../src/logic/Commands/columnCmd';
import { HandlerResult } from '../../src/types/schema-types';

export const setupSchema = (newTable:boolean = true) => {
    return new Schema(newTable ? 
        new Table("t",[],false) : {}, [], [],[],[],[], "schema stub")
}

describe('column Command', () => {
    it('exists', ()=>{
        expect(getCommands()["col"]).not.toBeUndefined();        
    })

    test('should require table to be focused', () => {
        const schema = setupSchema(false);

        const res:HandlerResult = columnCmdHandlerFunc(['User'], schema);

        expect(res.msg).toContain('must be a table')
    });

    test('adds a column', () => {
        const schema = setupSchema();

        const res:HandlerResult = columnCmdHandlerFunc(['author', 'number'], schema);
        res;

    });

    test('retruns message if not name and datatype', () => {
        const schema = setupSchema();

        const res:HandlerResult = columnCmdHandlerFunc(['author'], schema);

        expect(res.msg).toContain(`must`);
        expect(res.msg).toContain(`name`);
        expect(res.msg).toContain(`datatype`);
    });

    test('works with all the things', () => {
        const schema = setupSchema();

        const res:HandlerResult = columnCmdHandlerFunc(['author', 'integer', 'pk','d:yes', 'nn', 'r:cheese.fromage', 'ch:author<10'], schema);
        let column:Column = res.added;

        expect(res.msg).toContain(`added`);
        expect(column.constraint).toEqual("author<10");
        expect(column.comment).toContain("column for the t table");
        expect(column.datatype).toEqual("integer");
        expect(column.default).toEqual("yes");
        expect(column.name).toEqual("author");
        expect(column.notNullable).toBeTruthy();
        expect(column.parent.name).toEqual("t");
        expect(column.pk).toBeTruthy();
        expect(column.references).not.toBeUndefined();
        expect(column.references.table).toEqual("cheese");
        expect(column.references.column).toEqual("fromage");

        
    });
});
