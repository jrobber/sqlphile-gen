import './../../src/logic/Commands/tableCmd';
import '../../src/logic/logic'
import { getCommands } from '../../src/logic/logic';
import { Schema } from '../../src/logic/Classes';
import { tableCmdHandlerFunc } from './../../src/logic/Commands/tableCmd';
import { HandlerResult } from '../../src/types/schema-types';

export const setupSchema = () => {
    return new Schema({}, [], [],[],[],[], "schema stub")
}

describe('table Command', () => {
    it('exists', ()=>{
        expect(getCommands()["nt"]).not.toBeUndefined();        
    })
    test('should add table to schema', () => {
        let schema = setupSchema();

        const res:HandlerResult = tableCmdHandlerFunc(['User'], schema);

        expect(schema.tables.length).toBe(1);
        expect(res.msg).toBe("Table Added");
        expect(res.newFocus).not.toBeUndefined();
        expect(res.newFocus.name).toEqual('User');
        expect(res.newFocus.comment).toEqual('Where Users are stored');
        expect(res.allowsComment).toBeTruthy();
    });

    test('should give help if missing tablename', () => {
        let schema = setupSchema();

        const res:HandlerResult = tableCmdHandlerFunc([], schema);
        res;
        expect(schema.tables.length).toBe(0);

        expect(res.msg).toContain("Missing table name");
        expect(res.msg).toContain("TableName");
    });

    test('should mark table private', () => {
        let schema = setupSchema();

        const res:HandlerResult = tableCmdHandlerFunc(['User', 'p'], schema);
        
        expect(res.newFocus.isPrivate).toBeTruthy();
    });

    test('should ignore additional text', () => {
        let schema = setupSchema();

        const res:HandlerResult = tableCmdHandlerFunc(['User', 'p', 'Users are great'], schema);
        
        expect(schema.tables.length).toBe(1);
        expect(res.msg).toBe("Table Added");
        expect(res.newFocus).not.toBeUndefined();
        expect(res.newFocus.name).toEqual('User');
        expect(res.newFocus.comment).toEqual('Where Users are stored');
        expect(res.allowsComment).toBeTruthy();
    });
});
