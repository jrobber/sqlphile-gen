import {registerCommand, handleIt} from '../../src/logic/logic'
import { Command, Schema } from '../../src/logic/Classes'

export const setupSchema = () => {
    return new Schema({}, [], [],[],[],[], "schema stub")
}

describe('register Command', ()=> {
    it('registers single command', ()=>{
       let command = new Command("Do", ["do"], "help", ()=>{});

        let commands = registerCommand(command);

        expect(commands["do"].name).toEqual("Do")
    })

    it('registers multiple command triggers', ()=>{
       let command = new Command("Do", ["do", "doo"], "help", ()=>{});

        let commands = registerCommand(command);

        expect(commands["do"].name).toEqual("Do")
        expect(commands["doo"].name).toEqual("Do")
    })

    it('does not overwrite', ()=>{
       let command = new Command("Do", ["do", "doo"], "help", ()=>{});
        let command2 = new Command("So", ["so", "doo"], "help", ()=>{});

        registerCommand(command);
        let commands = registerCommand(command2);

        expect(commands["do"].name).toEqual("Do")
        expect(commands["doo"].name).toEqual("Do")
        expect(commands["so"].name).toEqual("So")
    })
})

describe('handle it', ()=>{
    it('invokes correct function', ()=>{
        let called = false;
        let command = new Command("hi1", ["hi1"], "help", ()=>{called = true});

        registerCommand(command);
        handleIt('hi1', setupSchema())

        expect(called).toEqual(true)        
    })

    it('invokes correct function with args', ()=>{
        let arg:string = '';
        let command = new Command("hi2", ["hi2", "hi2"], "help", (ar:string, schema:Schema)=>{arg = ar});

        registerCommand(command);
        handleIt('hi2 stuff',  setupSchema())

        expect(arg[0]).toEqual('stuff');
    })

    
    test('trims whitespace on args', () => {
        let arg:string = '';
        let command = new Command("hi3", ["hi3", "hi3"], "help", (ar:Array<string>, schema:Schema)=>{arg = ar});

        registerCommand(command);
        handleIt('hi3    stuffer  b d',  setupSchema())

        expect(arg[0]).toEqual('stuffer');
        expect(arg.length).toEqual(3);
    });
})