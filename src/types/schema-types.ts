export interface ITable {
    rows: Array<IColumn>;
    isPrivate: boolean;
    comment: string;
    name: string;
}

export interface IEnumTable {
    name: string;
    enums: Array<string>;
    isPrivate: boolean;
}

export interface ICommand {
    name: string;
    triggerText: Array<string>;
    handler: (arg: Array<string>, schema: ISchema) => HandlerResult;
}
export interface HandlerResult {
    msg: string,
    newFocus?: any,
    allowsComment?: boolean,
    added?: any
}

export interface IColumn {
    name: string;
    pk: boolean;
    constraint: string;
    comment: string;
    datatype: string;
    parent: ITable;
    default: string;
    notNullable: boolean;
    references?: ITableColumnPair;
}

export interface IReference {
    references: ITableColumnPair;
    referencing: ITableColumnPair;
    direction: string;
    cascade: boolean;
}


export interface IManyToManyReference extends IReference {
    tableInfo: ITable;
}

export interface IComputed {
    name: string;
    parameters: Array<ITableColumnPair>;
    returnType: string;
    comment: string;
    isPrivate: boolean;
    roles: Array<IRole>;
}

export interface ITableColumnPair {
    table: ITable;
    column: IColumn;
}

export interface IPolicy {
    crud: string;
    currentUser: boolean;
    roles: Array<IRole>
}

export interface ITrigger {
    table: ITable;
    crud: string
    column: IColumn;
    timing: string
}


export interface IAuth {
    accountTable: ITable;
    notJWT: boolean;
    roles: Array<IRole>;
    connectionPassword: string;
}

export interface IRole {
    name: string;
    extends: IRole;
}

export interface ISchema {
    focus: any,
    tables: Array<ITable>,
    computed: Array<IComputed>,
    roles: Array<IRole>,
    policies: Array<IPolicy>,
    auth: Array<IAuth>,
    name: string
}