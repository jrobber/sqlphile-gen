export const TriggerTiming = [
    "before",
    "after"
]

export const CrudOperations = [
    'update',
    'delete',
    'insert',
    'read'
]

export const ReferenceDirection = [
    'OneToMany',
    'ManyToMany',
    'ManyToOne',
    'OneToOne'
]

export const DataTypes = [
    'integer',
    'text',
    'timestamp',
    'serial',
    'float',    
]