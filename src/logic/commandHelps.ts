export const checkHelp = "Constraints must use a : and then provide a value IE - c:age>18 | check:char_length(headline)<80"

export const helps = {
    'c': checkHelp,
    'check': checkHelp,
    'con': checkHelp,
    'constraint': checkHelp,
}