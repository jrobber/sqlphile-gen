import { registerCommand } from "../logic";
import { TableColumnPair, Command, Schema, Table, Column } from "../Classes";
import { ITable } from './../../types/schema-types';
import { DataTypes } from "../lookups";

export const columnHelpStr = "c|col|column|nc ColumnName datatype [pk|primarykey|serial]? [d|default]:str_w_no_spaces [nn|notnull] [r|references]:tablename.columnname [ch|check|con|constraint]:str_expression & then press tab to add a comment"

registerCommand(new Command("add column", ["c", "col", "column", "nc"], columnHelpStr, columnCmdHandlerFunc))

export function columnCmdHandlerFunc(args: Array<string>, schema: Schema) {
    const isTable = schema.focus instanceof Table;
    if (!schema.focus || !isTable) {
        return {
            msg: `Current focus must be a table.  Try 'f t TableName' first`
        }
    }

    const errorMessages: Array<string> = [];
    args
    if (args.length < 2) {
        errorMessages.push(`Columns must have a name and a datatype at a minimum`)
    }

    const columnName = args[0];
    const dataType: string = args[1];
    if (DataTypes.indexOf(dataType) < 0) {
        errorMessages.push(`WARNING: Not a known postgres datatype`)
    }

    const optionalArgs = buildOptionalArgs(args, errorMessages);


    const newColumn = new Column(columnName,
        dataType,
        schema.focus,
        optionalArgs.pk,
        optionalArgs.constraint !== 'placeholder' ? optionalArgs.constraint : undefined,
        "",
        optionalArgs.default !== 'placeholder' ? optionalArgs.default : undefined,
        optionalArgs.notNullable,
        undefined
    )

    if (optionalArgs.references !== `placeholder`) {
        const pairSplit = optionalArgs.references.split('.')
        newColumn.references = new TableColumnPair(pairSplit[0], pairSplit[1]);
    }

    (schema.focus as ITable).rows.push(newColumn);

    return {
        msg: errorMessages.length > 0
            ? errorMessages.join('\r\n')
            : `Column ${newColumn.name} added to ${schema.focus.name}`,
        added: newColumn,
        allowsComment: true
    }
}

function buildOptionalArgs(args: Array<string>, errorMessages: Array<string>) {
    const oArgs: any = {
        pk: false,
        constraint: "placeholder",
        default: "placeholder",
        notNullable: false,
        references: "placeholder"
    }
    if (args.length < 3) return oArgs;

    const optionalArgs = args.slice(2);



    const argsStr = optionalArgs.join(' ').toLowerCase();
    oArgs.pk = argsStr.match(/pk|primarykey|serial/) ? true : false;
    setVal(/ch|check|con|constraint/, 'c', 'constraint', argsStr, oArgs, optionalArgs, errorMessages);
    setVal(/d|default/, 'd', 'default', argsStr, oArgs, optionalArgs, errorMessages);
    oArgs.notNullable = argsStr.match(/nn|notnull/) ? true : false;
    setVal(/r|references/, 'r', 'references', argsStr, oArgs, optionalArgs, errorMessages)

    oArgs;
    return oArgs;
}

function getVal(arr: Array<string>, charStart: string): string {
    const argArr = arr.filter((s: string) => s.startsWith(charStart));
    let arg = ":"
    if (argArr.length > 0) {
        arg = argArr[0];
        return arg.split(":")[1];
    } else {
        return "";
    }

}

function setVal(match: RegExp, startChar: string, key: string, argsStr: string, oArgs: any, optionalArgs: Array<string>, errorMessages: Array<string>) {
    const isMatch = argsStr.match(match);
    if (isMatch) {
        const val = getVal(optionalArgs, startChar);
        if (val) {
            oArgs[key] = val;
        }
        else {
            errorMessages.push(`Invalid struture provided to ${key}.  [key]:value_no_spaces`);
        }
    }
}