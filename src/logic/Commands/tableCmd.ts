import { registerCommand } from "../logic";
import { Command, Schema, Table } from "../Classes";


let tableHelpStr = "t|nt|table TableName [p|private]? then press tab to add a comment"

registerCommand(new Command("add table", ["t", "table", "nt"], tableHelpStr, tableCmdHandlerFunc))

export function tableCmdHandlerFunc(args: Array<string>, schema: Schema) {
    if (args.length < 1) {
        return {
            msg: `Missing table name: ${tableHelpStr}`
        }
    }

    const tableName = args[0].trim();
    args.shift(); 
    const match = args[0] && args[0].toLowerCase().match(/p|private|priv/);
    const isPrivate = match ? true : false;
    match && args.shift();
    const comment = `Where ${tableName}s are stored`

    if (tableName) {
        const newT = new Table(tableName, [], isPrivate);
        newT.comment = comment;
        schema.tables.push(newT)

        return {
            msg: "Table Added",
            newFocus: newT,
            allowsComment: true
        }
    }
    return {
        msg: `Invalid table handler path`
    }
}
