import { IColumn, ITable, ITableColumnPair } from "../../types/schema-types";

export default class Column implements IColumn {
    name: string;//
    pk: boolean;//
    constraint: string;
    comment: string;//
    datatype: string;//
    parent: ITable;//
    default: string;//
    notNullable: boolean;//
    references?: ITableColumnPair;//

	constructor(name: string, datatype: string, parent: ITable, pk?: boolean, constraint?: string, comment?: string,  defaultStr?: string, notNullable?: boolean, references?: ITableColumnPair) {
		this.name = name;
		this.pk = pk || false;
		this.constraint = constraint || "";
		this.comment = comment || `The ${name} column for the ${parent.name} table`;
		this.datatype = datatype;
		this.parent = parent;
		this.default = defaultStr || "";
		this.notNullable = notNullable || false;
		this.references = references ? references : undefined;
	}
    
}