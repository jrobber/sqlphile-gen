export default class Policy implements IPolicy {
    crud: CrudOperations;
    currentUser: boolean;
    roles: Array<IRole>;


    constructor(crud: CrudOperations, currentUser: boolean, roles: Array<IRole>) {
        this.crud = crud;
        this.currentUser = currentUser;
        this.roles = roles;
    }
}