export default class Computed implements IComputed {

    name: string;
    parameters: Array<ITableColumnPair>;
    returnType: DataType;
    comment: string;
    isPrivate: boolean;
    roles: Array<IRole>;
    
    constructor(name: string, parameters: Array<ITableColumnPair>, returnType: DataType, comment: string, isPrivate: boolean, roles: Array<IRole>) {
        this.name = name;
        this.parameters = parameters;
        this.returnType = returnType;
        this.comment = comment;
        this.isPrivate = isPrivate;
        this.roles = roles;
    }
}