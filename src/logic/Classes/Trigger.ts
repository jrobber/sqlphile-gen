

export default class Trigger implements ITrigger {
    table: ITable;
    crud: CrudOperations;
    column: IColumn;
    timing: TriggerTiming;


    constructor(table: ITable, crud: CrudOperations, column: IColumn, timing: TriggerTiming) {
        this.table = table;
        this.crud = crud;
        this.column = column;
        this.timing = timing;
    }

}