

export default class Schema implements ISchema {
    focus: any;
    tables: Array<ITable>;
    computed: Array<IComputed>;
    roles: Array<IRole>;
    policies: Array<IPolicy>;
    auth: Array<IAuth>;
    name: string;

    constructor(focus: any, tables: Array<ITable>, computed: Array<IComputed>, roles: Array<IRole>, policies: Array<IPolicy>, auth: Array<IAuth>, name: string) {
        this.focus = focus;
        this.tables = tables;
        this.computed = computed;
        this.roles = roles;
        this.policies = policies;
        this.auth = auth;
        this.name = name;
    }
}