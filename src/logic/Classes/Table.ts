export default class Table implements ITable {
    rows: Array<IColumn> = [];
    isPrivate: boolean = false;
    comment: string = "";
    name: string = "";

    /**
     *
     */
    constructor(name: string, rows: Array<IColumn>, isPrivate:boolean) {
        this.rows = rows;
        this.isPrivate = isPrivate;
        this.name = name;
    }    
}
