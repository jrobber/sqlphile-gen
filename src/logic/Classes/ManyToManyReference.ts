import Reference from './Reference'

export default class ManyToManyReference extends Reference {
    tableInfo: ITable;

    public constructor(tableInfo: ITable, references: ITableColumnPair, referencing: ITableColumnPair, direction: ReferenceDirection, cascade: boolean){
        super(references, referencing, direction, cascade);
        this.tableInfo = tableInfo;
    }
}