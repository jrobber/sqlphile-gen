

export default class Auth implements IAuth {
    accountTable: ITable;
    notJWT: boolean;
    roles: Array<IRole>;
    connectionPassword: string;


    constructor(accountTable: ITable, notJWT: boolean, roles: Array<IRole>, connectionPassword: string) {
        this.accountTable = accountTable;
        this.notJWT = notJWT;
        this.roles = roles;
        this.connectionPassword = connectionPassword;
    }

}
