export default class Reference implements IReference {
     references: ITableColumnPair;
     referencing: ITableColumnPair;
     direction: ReferenceDirection;
     cascade: boolean;

	constructor(references: ITableColumnPair, referencing: ITableColumnPair, direction: ReferenceDirection, cascade: boolean) {
		this.references = references;
		this.referencing = referencing;
		this.direction = direction;
		this.cascade = cascade;
	}
}