export default class TableColumnPair implements ITableColumnPair {
    table: ITable;
    column: IColumn;

    constructor(table: ITable, column: IColumn) {
        this.table = table;
        this.column = column;
    }

}
