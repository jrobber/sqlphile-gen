export default class EnumTable implements IEnumTable {
    name: string;
    enums: string[];
    isPrivate: boolean;
        

	constructor(name: string, enums: string[], isPrivate: boolean) {
		this.name = name;
		this.enums = enums;
		this.isPrivate = isPrivate;
	}    
}