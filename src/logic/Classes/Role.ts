
export default class Role implements IRole {
    name: string;
    extends: IRole;


    constructor(name: string, extendsRole: IRole) {
        this.name = name;
        this.extends = extendsRole;
    }

}
