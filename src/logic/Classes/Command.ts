import { ICommand, ISchema, HandlerResult } from "../../types/schema-types";

export default class Command implements ICommand {
     name: string;
     triggerText: Array<string>;
	 handler: (arg: Array<string>, schema:ISchema)=>HandlerResult;
	 helpStr: string;


	constructor(name: string, triggerText: 
		Array<string>,
		helpStr: string, 
		handler: (arg: Array<string>, schema:ISchema)=>HandlerResult,
		) {
		this.name = name;
		this.triggerText = triggerText;
		this.handler = handler;
		this.helpStr = helpStr;
	}
    
    
}
