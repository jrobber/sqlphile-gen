import { Schema } from './Classes/index';
let commands:any = {};
import * as _ from 'lodash'
import { ICommand } from '../types/schema-types';

export function registerCommand(command: ICommand) {
    _.without(command.triggerText, ...Object.keys(commands))
        .forEach((cmd) => {
            commands[cmd] = command
        })

    return commands;
}

export function getCommands(){
    return commands;
}

export function handleIt(commandText: string, schema: Schema) {
    let args = commandText.split(/\s+/)
    args;
    let c = commands[args[0]];
    args.shift()
    args = args.filter(x=>x !== '');    
    if(c) {
        return c.handler(args, schema)
    }
    return {
        msg: `Command undefined`
    }
}
